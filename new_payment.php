<?php

// Pido el access token
$url = "https://secure.payu.com/pl/standard/user/oauth/authorize";

$header = array("Content-Type: application/x-www-form-urlencoded", "Authorization: Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd");

// ojo, como el content-type cambió, capaz que la forma de cargar acá el contenido, también.
$content = "grant_type=client_credentials&client_id=145227&client_secret=12f071174cb7eb79d4aac5bc2f07563f";


$answer = post_request($url, $content, $header);

$token = $answer["access_token"];


// Acá puede haber un paso previo en el cual vos le pedís un token más donde, vos le tendrías que pasar, si existiere este paso:
// Lo que querés que el tipo te pague y el monto.
// Eso te respondería otro token que efectivamente, ese sí, lo usarías para generar la orden de pago de abajo...

// me explico rápido:
//$url=...
//content=...token=$token&client_id=145227&price=100&...&
//$answer = post_request(...
// $token = $answer["access_token"];
// fin del ejemplo

// creo la orden de pago

$content = '{
     "notifyUrl": "https://weegoo.com/notify/123",
     "customerIp": "127.0.0.1",
     "merchantPosId": "145227",
     "description": "RTV market",
     "currencyCode": "PLN",
     "totalAmount": "21000",
     "buyer": {
  "email": "john.doe@example.com",
             "phone": "654111654",
             "firstName": "John",
             "lastName": "Doe",
             "language": "en"
         },
     "settings":{
  "invoiceDisabled":"true"
         },
     "products": [
         {
           "name": "Wireless Mouse for Laptop",
             "unitPrice": "15000",
             "quantity": "1"
         },
         {
           "name": "HDMI cable",
             "unitPrice": "6000",
             "quantity": "1"
          }
        ]
     }';

?>

<!--  <form method="post" action="https://secure.payu.com/api/v2_1/orders">-->
<!--    <input type="hidden" name="customerIp" value="123.123.123.123">-->
<!--    <input type="hidden" name="merchantPosId" value="145227">-->
<!--    <input type="hidden" name="description" value="Order description">-->
<!--    <input type="hidden" name="totalAmount" value="1000">-->
<!--    <input type="hidden" name="currencyCode" value="PLN">-->
<!--    <input type="hidden" name="products[0].name" value="Product 1">-->
<!--    <input type="hidden" name="products[0].unitPrice" value="1000">-->
<!--    <input type="hidden" name="products[0].quantity" value="1">-->
<!--    <input type="hidden" name="notifyUrl" value="http://shop.url/notify">-->
<!--    <input type="hidden" name="continueUrl" value="--><?php //echo "http://weego.com/continue" ?><!--">-->
<!--    <input type="hidden" name="OpenPayu-Signature" value="sender=145227;algorithm=SHA-256;signature=bc94a8026d6032b5e216be112a5fb7544e66e23e68d44b4283ff495bdb3983a8">-->
<!--    <button type="submit" formtarget="_blank" >Pay with PayU</button>-->
<!--  </form >-->

<?php

$header = array("Content-type: application/json", "Authorization: Bearer $token");

$answer = post_request("https://secure.payu.com/api/v2_1/orders", $content, $header);


var_dump($answer);

print $answer;


function post_request($url, $content, $header){

// es muy probable que esto haya que reemplazarlo por un hash (array con strings como clave en PHP) y usar json_encode
//$your_data = array(
//  "notifyUrl" => "https://..."
//  //..
//);


//json_encode($your_data);

  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

  $json_response = curl_exec($curl);

  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  if ( $status != 201 ) {
    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
  }


  curl_close($curl);

  $response = json_decode($json_response, true);

  //var_dump($response);

  return $response;
}


